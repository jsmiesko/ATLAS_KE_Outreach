How setup Windows for ATLAS Monitor

. Uninstall McAfee
.. Open McAfee Center, disable all services, close center, ignore all messages about risk
.. run CCleaner/Tools/Uninstall McAfee - this take 3-4 minutes and needs restart
.. after restart close Firefox, open CCleaner/Tools/Startup/Firefox and disable McAfee plugin

Ban sleeping of display and computer
. Control Panel/Personalization/Screen Saver/ -check/set to None
. Control Panel/Power Optins/Turn off display/ 
.. Turn off the display - set to Never
.. Put computer to sleep - set to Never
.. Advanced setting - turn off password for wakeup, others set for maximum performance

Firefox - Automatic start after restart/turn on 
. In Start/All programs/Startup copy link of Firefox from Desktop and change Properties to next
.. General/Name: Firefox_reload
.. Shortcut/Target "C:\Program Files (x86)\Mozilla Firefox\firefox.exe" -p Reload -url file:C:\Users\eduard\Documents\LHCmon\reload5.htm -no-remote
.. Shortcut/Start in "C:\Program Files (x86)\Mozilla Firefox"

. Start Firefox over link in Start/All Programs/Startup and create new profile Reload
. Open page about:config and check/change to
.. app.update.auto;false
.. app.update.enabled;false
.. browser.startup.homepage;file:///C:/Users/Eduard/Documents/LHCmon/reload5.htm
.. services.sync.prefs.sync.browser.startup.homepage;false - dolezite
.. services.sync.prefs.sync.browser.startup.page;false     - dolezite
.. startup.homepage_welcome_url;https://www.google.com/

. Check all directory/path/images are correct (Picture/CERN_TXT ...)

After connect to TV, chcek WiFi settings and connect via USB