# ATLAS Košice Outreach

This repository contains outreach activities of ATLAS Košice, which were started
by Dušan Bruncko and Eduard Kladiva.


## ATLAS KE Outreach Machine

Name of the machine is `atlas-outreach.iep` with username `uefsav` and it is
accessible from internal IEP SAS KE network using following options:

* SSH:
  ```
  ssh uefsav@atlas-outreach.iep
  ```
* Cockpit:
  Direct your browser to:
  ```
  https://atlas-outreach.iep:9090
  ```
* VNC:
  ```
  vnc://atlas-outreach.iep
  ```

Additionally one can use `Monitorix` monitoring tool by directing browser to:
```
http://atlas-outreach:8080/monitorix
```

## ATLAS Outreach Videos

Playback of the CERN/ATLAS videos can be started with `play.sh` command.

Command for downloading CERN/ATLAS videos from CDS is `get.py`. It has one
parameter, the location of the JSON configuration file. For example:
```
./get.py CERN.json
```

The autostart script needs to be located in `.config/autostart/` directory and
be executable.


## ATLAS Monitor

This is backup of old system created by Eduard Kladiva, which showed current
status of LHC/ATLAS. It was running on Win 8. Backup of the disk is stored at:
