#!/bin/bash

while true; do
  mpv --fs \
      --playlist=/home/uefsav/ATLAS_KE_Outreach/Videos/CERN.pls \
      --external-file=/home/uefsav/ATLAS_KE_Outreach/Logos/Logo-Outline-web-White@150.png \
      --lavfi-complex="[vid1][vid2]overlay=60:60[vo]"

  mpv --fs \
      --playlist=/home/uefsav/ATLAS_KE_Outreach/Videos/ATLAS.pls \
      --external-file=/home/uefsav/ATLAS_KE_Outreach/Logos/ATLAS-Logo-BW-invert-RGB-H-trans-x-small.png \
      --lavfi-complex="[vid1][vid2]overlay=60:60[vo]"

  mpv --fs \
      --playlist=/home/uefsav/ATLAS_KE_Outreach/Videos/History.pls \
      --external-file=/home/uefsav/ATLAS_KE_Outreach/Logos/Logo-Outline-web-White@150.png \
      --lavfi-complex="[vid1][vid2]overlay=60:60[vo]"

  mpv --fs \
      --playlist=/home/uefsav/ATLAS_KE_Outreach/Videos/360video.pls \
      --external-file=/home/uefsav/ATLAS_KE_Outreach/Logos/Logo-Outline-web-White@150.png \
      --lavfi-complex="[vid1][vid2]overlay=60:60[vo]"
done
