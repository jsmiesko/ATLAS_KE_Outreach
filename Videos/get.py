#!/usr/bin/env python3
'''
Download all outreach videos.
'''

import sys
import json
import os

def main():
    ''' Main function. '''

    with open(sys.argv[1]) as infile:
        indict = json.load(infile)
        videos = indict['videos']

    with open(indict['playlist_name'], 'w') as outfile:
        outfile.write('[playlist]\n\n')
        n_videos = 0
        for index, video in enumerate(videos):
            command = 'cd "%s"; curl -O -J -L "%s"' % (indict['directory'],
                                                       video['url_video'])
            status = os.system(command)
            if os.WEXITSTATUS(status) == 0:
                outfile.write('File%i="%s/%s"\n' % (index + 1,
                                                    indict['directory'],
                                                    video['filename']))
                outfile.write('Title%i="%s"\n\n' % (index + 1, video['title']))
                n_videos += 1

        outfile.write('NumberOfEntries=%i\n' % (n_videos))
        outfile.write('Version=2\n')


if __name__ == '__main__':
    main()
